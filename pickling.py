import numpy as np
import pickle
import glob
import cv2

train_data = []
label_data = []


for x in glob.glob("C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\Data Tomat-4 Class\\Belum_Matang\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(0)

for x in glob.glob("C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\Data Tomat-4 Class\\Busuk\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(1)

for x in glob.glob("C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\Data Tomat-4 Class\\Hampir_Matang\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(2)

for x in glob.glob("C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\Data Tomat-4 Class\\Matang\\*.jpg"):
    img = cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)
    resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
    train_data.append((resized))
    label_data.append(3)
    

data_tomat = {"img_data" : np.array(train_data), "label_data" : np.array(label_data)}
print(data_tomat["img_data"].shape)
output = open('C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\DataTomat.p','wb')
pickle.dump(data_tomat,output)
output.close()

csvWrite = open('C:\\Users\\nicla\\Desktop\\Skripsi\\VSC\\DataTomat.csv','w')
columnTitleRow = "ClassID,Class\n"
csvWrite.write(columnTitleRow)
csvWrite.write("0,BELUM_MATANG\n")
csvWrite.write("1,BUSUK\n")
csvWrite.write("2,HAMPIR_MATANG\n")
csvWrite.write("3,MATANG\n")
csvWrite.close()
